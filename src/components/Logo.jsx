// importet le style du logo
import styles from './Logo.module.css';

export default function Logo() {
    return  <div className={styles.logo}>
        <img src={require('../resources/you2.png')} alt="Not me !" width="80%" ></img>
    </div>  
}