// importer le css du Header
import styles from './Header.module.css';

// importer mes composants
import Logo from './Logo';
import MenuNav from './MenuNav';
import Footer from './Footer';

export default function Header(props) {
    // retourner l'HTML du Header 
    return <header className={styles.header}>
        
        <Logo/>

        <MenuNav changePage={props.changePage} />

        <Footer/>

    </header>
}