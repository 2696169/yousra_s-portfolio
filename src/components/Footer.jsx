// importet le style de Footer
import styles from './Footer.module.css';

export default function Footer() {
    return  <>
        <p className={styles.copyright}>
            &copy; Copyright &copy; <br/> 2022 All rights reserved <br/> 
            This template is made with &#9829; <br/>
            By Yousra Merzouk
        </p>
    </>  
}