// importet le style de la photo
import styles from './Photo.module.css';

export default function Photo() {
    return  <div className={styles.photo}>
        <img src={require('../resources/modifier.png')} alt="Not me !" width="300px" height="300px"/>
    </div>  
}