// importer le style de MenuNav
import styles from './MenuNav.module.css';

export default function MenuNav(props) {
    return <>
        <ul className={styles.list}>
            <li className={styles.element}>
                <button className={styles.link} onClick={props.changePage('Accueil')}>Accueil</button>
            </li>
            <li className={styles.element}>
                <button className={styles.link} onClick={props.changePage('Projet1')}>Projet 1</button>
            </li>
            <li className={styles.element}>
                <button className={styles.link} onClick={props.changePage('Projet2')}>Projet 2</button>
            </li>
            <li className={styles.element}>
                <button className={styles.link} onClick={props.changePage('Contact')}>Contact</button>
            </li>
            <ul className={styles.reseau} >
                <li className={styles.element}>
                    <a className={styles.link} href="https://www.facebook.com/youyou.chitna.1/">
                        <img className={styles.img} src={require('../resources/facebook.png')} alt='facebook' />
                    </a>
                </li>
                <li className={styles.element}>
                    <a className={styles.link} href="https://www.instagram.com/y_ou_sra_7/">
                        <img className={styles.img} src={require('../resources/instagram.png')} alt='instagram'/>
                    </a>
                </li>
            </ul>
        </ul>
        

    </>
}