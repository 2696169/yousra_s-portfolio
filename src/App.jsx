import './App.css';
import { useState } from 'react';

// Importer mes composants
import Header from './components/Header';
import Accueil from './pages/Accueil';
import Contact from './pages/Contact';
import Projet1 from './pages/Projet1';
import Projet2 from './pages/Projet2';

export default function App() {
    const [pageCourante, setPageCourante] = useState('Accueil');

    const changePage = (page) => {
        return () => {
            setPageCourante(page);
        }
    }

    return <>
        <div className='navigateur'>
            <Header changePage={changePage} />
        </div>
        
        <div className='pages'>
            {pageCourante === 'Accueil' &&
                <Accueil />
            }
            
            {pageCourante === 'Projet1' &&
                <Projet1 />
            }

            {pageCourante === 'Projet2' &&
                <Projet2 />
            }
            
            {pageCourante === 'Contact' &&
                <Contact />
            }
        </div>
    </>
}


