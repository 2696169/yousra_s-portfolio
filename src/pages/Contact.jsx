
export default function Contact() {
    return <>
        <div>
            <div>
                <h2 className="titre">Mes Informations</h2>
            </div>
            <div>
                <p><span>Adresse:</span> Quelque part dans le monde</p>
            </div>
            <div>
                <p><span>Téléphone:</span> + 1234 567 89</p>
            </div>
            <div>
                <p><span>E-mail:</span> merzouk.yousra2020@gmail.com</p>
            </div>
        </div>

        <div>
            <div>
                <h2>Vos Informations</h2>
            </div>
            <form>
                <div>
                    <input type="text" className="form-control" placeholder="Votre Nom"/>
                </div>
                <div>
                    <input type="text" className="form-control" placeholder="Votre Email"/>
                </div>
                <div>
                    <input type="text" className="form-control" placeholder="Sujet"/>
                </div>
                <div>
                    <textarea className="form-control-textarea" placeholder="Message" ></textarea>
                </div>
                <div>
                    <input type="submit" value="Envoyer Le Message" className="bouton"/>
                </div>
            </form>            
        </div>
            
    </>
}
