import Photo from '../components/Photo';
import ContentToggler from '../components/ContentToggler';

export default function Accueil() {
    return <div className='div-accueil' >
        
        <div>
            <Photo/>
        </div>

        <div>
            <h1 className='titre1'> THIS IS ME ! </h1>
            <h2> &#9819; Yousra Merzouk &#9819; </h2> 
            <p>
                Étudiante en programmation informatique au collége La Cité.<br/>
                Ce que j'aime le plus dans ce domaine, c'est le web et l'aspect du design.
            </p>
            <p><br/>
                Ce portfolio a pour but de présenter les différents projets sur lesquels j'ai travaillé. 
                Vous pouvez également visualiser mon résumé juste aprés.
            </p>
            <p><br/>
                Si vous avez une question, ou si vous avez simplement besoin d'un renseignement, 
                vous pouvez me contacter en cliquant sur : Contact &#9787; .
            </p>
        </div>
        
        <div>
            <ContentToggler title="Résumé"  >
                <ul>
                    <label>Information générales : </label>
                    <li>Date de naissance : En une journée froide du mois de décembre 1997 </li>
                    <li>Adresse : Quelque part dans le monde </li>
                    <li>E-mail : merzouk.yousra2020@gmail.com</li>
                    <li>Nationnalité : Kabyle Algérienne</li>
                </ul>
                <ul>
                    <label>Langues : </label>
                    <li>Kabyle &amp; Arabe (Native)</li>
                    <li>Français (Professionelle d'aprés mon amie)</li>
                    <li>Anglais (Hi and By) </li>
                </ul>
                <ul>
                    <label>Compétences techniques : </label>
                    <li>C / C++ / C#</li>
                    <li>Java</li>
                    <li>Html / Css </li>
                    <li>JavaScript / Php </li>
                    <li>Perl</li>
                    <li>Python</li>
                    <li>Sql / MySql / NoSql</li>
                </ul>
                <ul>
                    <label>Loisirs : </label>
                    <li>Voyager</li>
                    <li>Dessiner</li>
                    <li>Lire les BD</li>
                    <li>Accro aux dramas </li>
                </ul>
            </ContentToggler> 
        </div>
       
    
    </div>
}