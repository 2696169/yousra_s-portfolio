
import ContentToggler from '../components/ContentToggler';

export default function Projet2() {
    return <>
        <div>
            <h2> Description </h2>
            <p>
                Un site web d'un magasin en ligne.<br/> Spécialisé dans la vente de produits reliés aux dessins animés, aux mangas et à la culture japonaise.<br/> Langage utilisé : Html / Css / JavaScript.
            </p>
        </div>
       <div>
            <ContentToggler title="Images">
                <div>
                    <img src={require('../resources/projet2/img1.png')} alt="img1" width="80%"/>
                </div>
                <div>
                    <img src={require('../resources/projet2/img2.png')} alt="img2" width="80%"/>
                </div>
                <div>
                    <img src={require('../resources/projet2/img3.png')} alt="img3" width="80%"/>
                </div>
                <div>
                    <img src={require('../resources/projet2/img4.png')} alt="img4" width="80%"/>
                </div>
                <div>
                    <img src={require('../resources/projet2/img5.png')} alt="img5" width="80%"/>
                </div>
            </ContentToggler> 
        </div>
    
    </>
}
