
import ContentToggler from '../components/ContentToggler';

export default function Projet1() {
    return <>
        <div>
            <h2> Description </h2>
            <p>
                Une application bureau pour un collége. <br/> Permet de gérer les stagiaires qui appartiennent à des programmes différents. <br/> Langage utilisé : C# (WPF).
            </p>
        </div>
       <div>
            <ContentToggler title="Images"  >
                <div>
                    <img src={require('../resources/projet1/img1.png')} alt="img1" width="60%"/>
                </div>
                <div>
                    <img src={require('../resources/projet1/img2.png')} alt="img2" width="60%"/>
                </div>
                <div>
                    <img src={require('../resources/projet1/img3.png')} alt="img3" width="60%"/>
                </div>
                <div>
                    <img src={require('../resources/projet1/img4.png')} alt="img4" width="60%"/>
                </div>
            </ContentToggler> 
        </div>
    
    </>
}
